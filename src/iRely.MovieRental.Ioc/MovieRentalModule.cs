﻿using FluentValidation;
using iRely.MovieRental.Application.Books.Interfaces;
using iRely.MovieRental.Application.Books.Validators;
using iRely.MovieRental.Application.Customers.Interfaces;
using iRely.MovieRental.Application.Customers.Validators;
using iRely.MovieRental.Application.Books.Commands;
using iRely.MovieRental.Application.Customers.Commands;
using iRely.MovieRental.Infrastructure.Data;
using iRely.MovieRental.Infrastructure.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;
using iRely.MovieRental.Application.Transactions.Interfaces;

namespace iRely.MovieRental.Ioc;

public static class MovieRentalModule
{
    public static void AddMovieRentalModule(this IServiceCollection services)
    {
        services.AddScoped<ICustomerRepository, CustomerRepository>();
        services.AddScoped<IBookRepository, BookRepository>();
        services.AddScoped<ITransactionRepository, TransactionRepository>();

        services.AddScoped<IValidator<CreateCustomerCommand>, CreateCustomerCommandValidator>();
        services.AddScoped<IValidator<CreateBookCommand>, CreateBookCommandValidator>();
        services.AddScoped<IValidator<RentBookToCustomerCommand>, RentBookToCustomerCommandValidator>();
        services.AddScoped<IValidator<ReturnBookCommand>, ReturnBookCommandValidator>();
    }

    public static void AddMovieRentalDbContext(this IServiceCollection services)
    {
        services.AddDbContext<MovieRentalDbContext>();
    }
}
