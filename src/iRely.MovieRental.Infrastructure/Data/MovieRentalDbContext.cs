﻿using iRely.MovieRental.Core.Books;
using iRely.MovieRental.Core.Customers;
using iRely.MovieRental.Core.Transactions;
using Microsoft.EntityFrameworkCore;

namespace iRely.MovieRental.Infrastructure.Data;

public class MovieRentalDbContext : DbContext
{
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Book> Books { get; set; }
    public DbSet<Transaction> Transactions { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(MovieRentalDbContext).Assembly);
        base.OnModelCreating(modelBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.EnableSensitiveDataLogging();
        optionsBuilder.EnableDetailedErrors();

        var connectionString = "Server=localhost;User=root;Password=root;Database=irely_movierental;";

        optionsBuilder.UseMySql(connectionString, new MySqlServerVersion(new Version(8, 0, 23)));
    }
}
