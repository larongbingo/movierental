﻿using iRely.MovieRental.Application.Shared.Interfaces;
using iRely.MovieRental.Core.Shared.Interfaces;

namespace iRely.MovieRental.Infrastructure.Data.Repositories;

public abstract class RepositoryBase<T> : IRepository<T> where T : IEntity
{
    protected readonly MovieRentalDbContext _dbContext;

    public RepositoryBase(MovieRentalDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<int> SaveChangesAsync() => await _dbContext.SaveChangesAsync();
    public void Add(T entity) => _dbContext.Add(entity);
    public void Update(T entity) => _dbContext.Update(entity);

    public void Dispose()
    {
        _dbContext.Dispose();
        GC.SuppressFinalize(this);
    }
}
