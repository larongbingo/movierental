﻿using iRely.MovieRental.Application.Customers.Interfaces;
using iRely.MovieRental.Core.Customers;
using Microsoft.EntityFrameworkCore;

namespace iRely.MovieRental.Infrastructure.Data.Repositories;

public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
{
    public CustomerRepository(MovieRentalDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<bool> CustomerExistsByIdAsync(Guid id)
        => await _dbContext.Customers.AnyAsync(x => x.Id == id).ConfigureAwait(false);

    public async Task<bool> CustomerExistsByNameAsync(string name)
        => await _dbContext.Customers.AsNoTracking().AnyAsync(x => x.Name == name).ConfigureAwait(false);

    public async Task<IEnumerable<Customer>> GetAllCustomersAsync()
        => await _dbContext.Customers.AsNoTracking().ToListAsync().ConfigureAwait(false);

    public async Task<Customer?> GetCustomerByIdAsync(Guid id)
        => await _dbContext.Customers.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
}
