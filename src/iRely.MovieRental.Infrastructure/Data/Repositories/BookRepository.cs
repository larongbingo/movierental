﻿using iRely.MovieRental.Application.Books.Interfaces;
using iRely.MovieRental.Core.Books;
using Microsoft.EntityFrameworkCore;

namespace iRely.MovieRental.Infrastructure.Data.Repositories;

public class BookRepository : RepositoryBase<Book>, IBookRepository
{
    public BookRepository(MovieRentalDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<bool> BookExistsByIdAsync(Guid id)
        => await _dbContext.Books.AnyAsync(x => x.Id == id).ConfigureAwait(false);

    public async Task<bool> BookExistsByTitleAsync(string title)
        => await _dbContext.Books.AnyAsync(x => x.Title == title).ConfigureAwait(false);

    public async Task<IEnumerable<Book>> GetAllBooksAsync()
        => await _dbContext.Books.AsNoTracking().ToListAsync().ConfigureAwait(false);

    public async Task<Book?> GetBookByIdAsync(Guid id)
    {
        var book = await _dbContext.Books.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);

        if (book is null)
            return null;

        await _dbContext.Entry(book).Reference(b => b.RentingCustomer).LoadAsync().ConfigureAwait(false);
        
        return book;
    }
}
