﻿using iRely.MovieRental.Application.Transactions.Interfaces;
using iRely.MovieRental.Core.Transactions;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace iRely.MovieRental.Infrastructure.Data.Repositories;

public class TransactionRepository : RepositoryBase<Transaction>, ITransactionRepository
{
    public TransactionRepository(MovieRentalDbContext dbContext) : base(dbContext)
    {
    }

    public async Task<IEnumerable<Transaction>> GetTransactionsByBookId(Guid bookId)
        => await _dbContext.Transactions.Where(t => t.Books.Any(b => b.Id == bookId)).ToListAsync();

    public async Task<IEnumerable<Transaction>> GetTransactionsByCustomerId(Guid customerId)
        => await _dbContext.Transactions.Where(t => t.Customer.Id == customerId).ToListAsync();
}
