﻿using iRely.MovieRental.Core.Books;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iRely.MovieRental.Infrastructure.Data.Configuration;

public class BookConfiguration : IEntityTypeConfiguration<Book>
{
    public void Configure(EntityTypeBuilder<Book> builder)
    {
        builder.HasKey(b => b.Id);

        builder.HasOne(b => b.RentingCustomer)
            .WithMany()
            .HasForeignKey(b => b.RentingCustomerId);
    }
}
