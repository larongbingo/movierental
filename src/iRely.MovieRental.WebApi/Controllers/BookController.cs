﻿using iRely.MovieRental.Application.Books.Interfaces;
using iRely.MovieRental.Application.Books.Commands;
using Microsoft.AspNetCore.Mvc;
using MediatR;

namespace iRely.MovieRental.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IBookRepository _bookRepository;

        public BookController(IMediator mediator,
            IBookRepository bookRepository)
        {
            _mediator = mediator;
            _bookRepository = bookRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateBookCommand command)
        {
            var result = await _mediator.Send(command);

            return result.Match<IActionResult>(
                id => Ok(id),
                messages => BadRequest(messages));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
            => Ok(await _bookRepository.GetAllBooksAsync());

        [Route("rent")]
        [HttpPost]
        public async Task<IActionResult> RentBook(RentBookToCustomerCommand command)
        {
            var result = await _mediator.Send(command);
            return result.Match<IActionResult>(
                res => Ok(res),
                messages => BadRequest(messages));
        }

        [Route("return")]
        [HttpPost]
        public async Task<IActionResult> ReturnBook(ReturnBookCommand command)
        {
            var result = await _mediator.Send(command);
            return result.Match<IActionResult>(
                res => Ok(res),
                messages => BadRequest(messages));
        }
        [Route("{id}")]
        [HttpGet]
        public async Task<IActionResult> GetBookById([FromRoute] Guid id)
        {
            var book = await _bookRepository.GetBookByIdAsync(id);
            if (book is null)
                return BadRequest();
            else
                return Ok(book);
        }
    }
}
