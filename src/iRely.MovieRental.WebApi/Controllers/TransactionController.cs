﻿using iRely.MovieRental.Application.Transactions.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace iRely.MovieRental.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : Controller
    {
        private readonly ITransactionRepository _transactionRepository;

        public TransactionController(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        [Route("book/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetTransactionsByBookId([FromRoute] Guid id)
        {
            return Ok(await _transactionRepository.GetTransactionsByBookId(id));
        }

        [Route("customer/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetTransactionByCustomerId([FromRoute] Guid id)
        {
            return Ok(await _transactionRepository.GetTransactionsByCustomerId(id));
        }
    }
}
