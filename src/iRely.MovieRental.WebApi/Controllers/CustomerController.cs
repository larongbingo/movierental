﻿using iRely.MovieRental.Application.Customers.Interfaces;
using iRely.MovieRental.Application.Customers.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace iRely.MovieRental.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ICustomerRepository _customerRepository;

        public CustomerController(IMediator mediator,
            ICustomerRepository customerRepository)
        {
            _mediator = mediator;
            _customerRepository = customerRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateCustomerCommand command)
        {
            var result = await _mediator.Send(command);
            return result.Match<IActionResult>(
                id => Ok(id),
                messages => BadRequest(messages));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
            => Ok(await _customerRepository.GetAllCustomersAsync());
    }
}
