﻿namespace iRely.MovieRental.Core.Shared.Interfaces;
public interface IEntity
{
    public Guid Id { get; set; }
}