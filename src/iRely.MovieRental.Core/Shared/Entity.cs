using iRely.MovieRental.Core.Shared.Interfaces;

namespace iRely.MovieRental.Core.Shared;

public abstract class Entity : IEntity
{
    public Guid Id { get; set; }

    public DateTimeOffset CreatedAt { get; private set; } = DateTimeOffset.UtcNow;
    public DateTimeOffset UpdatedAt { get; set; } = DateTimeOffset.UtcNow;
    public DateTimeOffset? DeletedAt { get; set; }
}