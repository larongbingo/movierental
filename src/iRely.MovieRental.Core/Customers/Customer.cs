using iRely.MovieRental.Core.Books;
using iRely.MovieRental.Core.Shared;

namespace iRely.MovieRental.Core.Customers;

public class Customer : Entity
{
    public string Name { get; set; }
    
    public Customer(string name)
    {
        Name = name;
    }
}