﻿using iRely.MovieRental.Core.Customers;
using iRely.MovieRental.Core.Shared;

namespace iRely.MovieRental.Core.Books;

public class Book : Entity
{
    public string Title { get; set; }
    public string Description { get; set; }
    public Uri? DetailsUri { get; set; }
    public Guid? RentingCustomerId { get; set; }
    public virtual Customer? RentingCustomer { get; set; }

    public Book(string title, string description, Uri? detailsUri = null)
    {
        Title = title;
        Description = description;
        DetailsUri = detailsUri;
    }

    public void Rent(Customer customer)
        => RentingCustomer = customer;

    public void Return()
        => RentingCustomer = null;
}
