﻿namespace iRely.MovieRental.Core.Transactions;

public enum TransactionType
{
    Rent,
    Return
}
