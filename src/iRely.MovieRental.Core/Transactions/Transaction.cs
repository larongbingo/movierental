﻿using iRely.MovieRental.Core.Books;
using iRely.MovieRental.Core.Customers;
using iRely.MovieRental.Core.Shared;

namespace iRely.MovieRental.Core.Transactions;

public class Transaction : Entity
{
    public TransactionType TransactionType { get; set; }
    public virtual Customer Customer { get; set; }
    public virtual IEnumerable<Book> Books { get; set; }
    public Transaction(TransactionType transactionType, Customer customer, IEnumerable<Book> books)
    {
        TransactionType = transactionType;
        Customer = customer;
        Books = books;
    }
    public Transaction() { }
}
