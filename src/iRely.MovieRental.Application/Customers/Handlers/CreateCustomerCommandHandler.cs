﻿using FluentValidation;
using iRely.MovieRental.Application.Customers.Interfaces;
using iRely.MovieRental.Application.Shared;
using iRely.MovieRental.Application.Customers.Commands;
using MediatR;

namespace iRely.MovieRental.Application.Customers.Handlers;

public class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, Result<Guid, ValidationMessages>>
{
    private readonly IValidator<CreateCustomerCommand> _validator;
    private readonly ICustomerRepository _customerRepository;

    public CreateCustomerCommandHandler(IValidator<CreateCustomerCommand> validator, ICustomerRepository customerRepository)
    {
        _validator = validator;
        _customerRepository = customerRepository;
    }

    public async Task<Result<Guid, ValidationMessages>> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
    {
        var validationResult = await _validator.ValidateAsync(request);

        if (validationResult.IsValid)
        {
            var customer = request.CommandToEntity();
            _customerRepository.Add(customer);
            await _customerRepository.SaveChangesAsync();
            return customer.Id;
        }

        return new ValidationMessages(validationResult.Errors.Select(x => x.ErrorMessage));
    }
}
