﻿using iRely.MovieRental.Application.Customers.Interfaces;
using iRely.MovieRental.Application.Customers.Commands;

namespace iRely.MovieRental.Application.Customers.Validators;

public class CreateCustomerCommandValidator : CustomerCommandValidatorBase<CustomerCommandBase>
{
    public CreateCustomerCommandValidator(ICustomerRepository customerRepository) : base(customerRepository)
    {
    }
}
