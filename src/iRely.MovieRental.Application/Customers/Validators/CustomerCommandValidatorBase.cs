﻿using FluentValidation;
using iRely.MovieRental.Application.Customers.Interfaces;
using iRely.MovieRental.Application.Customers.Commands;

namespace iRely.MovieRental.Application.Customers.Validators;

public abstract class CustomerCommandValidatorBase<T> : AbstractValidator<T> where T : CustomerCommandBase
{
    private readonly ICustomerRepository _customerRepository;

    protected CustomerCommandValidatorBase(ICustomerRepository customerRepository)
    {
        _customerRepository = customerRepository;
        ValidateNameIsUnique();
        ValidateName();
    }

    private void ValidateNameIsUnique()
    {
        RuleFor(customerCommandBase => customerCommandBase.Name)
            .MustAsync(async (name, cancellationToken) => !(await _customerRepository.CustomerExistsByNameAsync(name)))
            .WithSeverity(Severity.Error)
            .WithMessage("A customer with this name already exists");
    }

    private void ValidateName()
    {
        RuleFor(customerCommandBase => customerCommandBase.Name)
            .Must(name => !string.IsNullOrEmpty(name))
            .WithSeverity(Severity.Error)
            .WithMessage("Name can't be empty");
    }
}
