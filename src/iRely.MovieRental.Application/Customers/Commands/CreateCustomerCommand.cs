﻿using iRely.MovieRental.Application.Shared;
using MediatR;

namespace iRely.MovieRental.Application.Customers.Commands;

public class CreateCustomerCommand : CustomerCommandBase, IRequest<Result<Guid, ValidationMessages>>
{
}
