namespace iRely.MovieRental.Application.Customers.Commands;

public abstract class CustomerCommandBase
{
    public string Name { get; set; }
}