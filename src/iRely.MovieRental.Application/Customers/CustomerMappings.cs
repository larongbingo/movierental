﻿using iRely.MovieRental.Application.Customers.Commands;

namespace iRely.MovieRental.Application.Customers;

public static class CustomerMappings
{
    public static Core.Customers.Customer CommandToEntity(this CreateCustomerCommand command)
        => new(command.Name);
}
