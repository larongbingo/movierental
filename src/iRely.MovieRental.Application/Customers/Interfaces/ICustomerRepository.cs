﻿using iRely.MovieRental.Application.Shared.Interfaces;
using iRely.MovieRental.Core.Customers;

namespace iRely.MovieRental.Application.Customers.Interfaces;

public interface ICustomerRepository : IRepository<Customer>
{
    Task<bool> CustomerExistsByNameAsync(string name);
    Task<bool> CustomerExistsByIdAsync(Guid id);
    Task<Customer?> GetCustomerByIdAsync(Guid id);
    Task<IEnumerable<Customer>> GetAllCustomersAsync();
}
