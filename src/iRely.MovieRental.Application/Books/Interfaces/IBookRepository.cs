﻿using iRely.MovieRental.Application.Shared.Interfaces;
using iRely.MovieRental.Core.Books;

namespace iRely.MovieRental.Application.Books.Interfaces;

public interface IBookRepository : IRepository<Book>
{
    Task<bool> BookExistsByTitleAsync(string title);
    Task<bool> BookExistsByIdAsync(Guid id);
    Task<Book?> GetBookByIdAsync(Guid id);
    Task<IEnumerable<Book>> GetAllBooksAsync();
}
