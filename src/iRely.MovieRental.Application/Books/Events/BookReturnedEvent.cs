﻿using iRely.MovieRental.Core.Books;
using iRely.MovieRental.Core.Customers;
using MediatR;

namespace iRely.MovieRental.Application.Books.Events;

public class BookReturnedEvent : INotification
{
    public Customer Customer { get; set; }
    public Book Book { get; set; }
    public BookReturnedEvent(Customer customer, Book book)
    {
        Customer = customer;
        Book = book;
    }
}