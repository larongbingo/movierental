﻿using FluentValidation;
using iRely.MovieRental.Application.Books.Commands;
using iRely.MovieRental.Application.Books.Interfaces;

namespace iRely.MovieRental.Application.Books.Validators;

public class ReturnBookCommandValidator : AbstractValidator<ReturnBookCommand>
{
    private readonly IBookRepository _bookRepository;
    
    public ReturnBookCommandValidator(IBookRepository bookRepository)
    {
        _bookRepository = bookRepository;
        ValidateBookIdIfExists();
        ValidateBookIdIfRented();
    }

    private void ValidateBookIdIfExists()
    {
        RuleFor(command => command.BookId)
            .MustAsync(async (bookId, cancellationToken) => await _bookRepository.BookExistsByIdAsync(bookId).ConfigureAwait(false))
            .WithSeverity(Severity.Error)
            .WithMessage("The given bookId doesn't exist");
    }

    private void ValidateBookIdIfRented()
    {
        RuleFor(command => command.BookId)
            .MustAsync(async (bookId, cancellationToken) => (await _bookRepository.GetBookByIdAsync(bookId).ConfigureAwait(false)).RentingCustomer is not null)
            .WithSeverity(Severity.Error)
            .WithMessage("The given book is not rented to any customer");
    }
}
