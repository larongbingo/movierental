﻿using iRely.MovieRental.Application.Books.Interfaces;
using iRely.MovieRental.Application.Books.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRely.MovieRental.Application.Books.Validators;

public class CreateBookCommandValidator : BookCommandValidatorBase<CreateBookCommand>
{
    public CreateBookCommandValidator(IBookRepository bookRepository) : base(bookRepository)
    {
    }
}
