﻿using FluentValidation;
using iRely.MovieRental.Application.Books.Commands;
using iRely.MovieRental.Application.Books.Interfaces;

namespace iRely.MovieRental.Application.Books.Validators;

public abstract class BookCommandValidatorBase<T> : AbstractValidator<T> where T : BookCommandBase
{
    private readonly IBookRepository _bookRepository;

    protected BookCommandValidatorBase(IBookRepository bookRepository)
    {
        _bookRepository = bookRepository;
        ValidateTitleIsUnique();
        ValidateTitle();
        ValidateDescription();
    }

    private void ValidateTitleIsUnique()
    {
        RuleFor(bookCommandBase => bookCommandBase.Title)
            .MustAsync(async (title, cancellationToken) => !(await _bookRepository.BookExistsByTitleAsync(title)))
            .WithSeverity(Severity.Error)
            .WithMessage("A book with this title already exists");
    }

    private void ValidateTitle()
    {
        RuleFor(bookCommandBase => bookCommandBase.Title)
            .Must(title => !string.IsNullOrEmpty(title))
            .WithSeverity(Severity.Error)
            .WithMessage("Title can't be empty");
    }

    private void ValidateDescription()
    {
        RuleFor(bookCommandBase => bookCommandBase.Description)
            .Must(description => !string.IsNullOrEmpty(description))
            .WithSeverity(Severity.Error)
            .WithMessage("Description can't be empty");
    }
}
