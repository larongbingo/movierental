﻿using FluentValidation;
using iRely.MovieRental.Application.Books.Interfaces;
using iRely.MovieRental.Application.Customers.Interfaces;
using iRely.MovieRental.Application.Books.Commands;

namespace iRely.MovieRental.Application.Books.Validators;

// TODO: allow multiple books to be rented
public class RentBookToCustomerCommandValidator : AbstractValidator<RentBookToCustomerCommand>
{
    private readonly IBookRepository _bookRepository;
    private readonly ICustomerRepository _customerRepository;

    public RentBookToCustomerCommandValidator(
        IBookRepository bookRepository,
        ICustomerRepository customerRepository)
    {
        _bookRepository = bookRepository;
        _customerRepository = customerRepository;
        ValidateCustomerIdIfExists();
        ValidateBookIdIfExists();
    }

    private void ValidateCustomerIdIfExists()
    {
        RuleFor(command => command.CustomerId)
            .MustAsync(async (customerId, _) => await _customerRepository.CustomerExistsByIdAsync(customerId).ConfigureAwait(false))
            .WithSeverity(Severity.Error)
            .WithMessage("The given customerId doesn't exist");
    }

    private void ValidateBookIdIfExists()
    {
        RuleFor(command => command.BookId)
            .MustAsync(async (bookId, _) => await _bookRepository.BookExistsByIdAsync(bookId).ConfigureAwait(false))
            .WithSeverity(Severity.Error)
            .WithMessage("The given bookId doesn't exist");
    }
}
