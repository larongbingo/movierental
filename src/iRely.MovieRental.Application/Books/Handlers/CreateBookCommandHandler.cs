﻿using FluentValidation;
using iRely.MovieRental.Application.Books.Commands;
using iRely.MovieRental.Application.Books.Interfaces;
using iRely.MovieRental.Application.Shared;
using MediatR;

namespace iRely.MovieRental.Application.Books.Handlers;

public class CreateBookCommandHandler : IRequestHandler<CreateBookCommand, Result<Guid, ValidationMessages>>
{
    private readonly IValidator<CreateBookCommand> _validator;
    private readonly IBookRepository _bookRepository;

    public CreateBookCommandHandler(IValidator<CreateBookCommand> validator, IBookRepository bookRepository)
    {
        _validator = validator;
        _bookRepository = bookRepository;
    }

    public async Task<Result<Guid, ValidationMessages>> Handle(CreateBookCommand request, CancellationToken cancellationToken)
    {
        var validationResult = await _validator.ValidateAsync(request);

        if (validationResult.IsValid)
        {
            var book = request.CommandToEntity();
            _bookRepository.Add(book);
            await _bookRepository.SaveChangesAsync();
            return book.Id;
        }

        return new ValidationMessages(validationResult.Errors.Select(x => x.ErrorMessage));
    }
}