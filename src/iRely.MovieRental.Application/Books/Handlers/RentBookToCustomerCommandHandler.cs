﻿using FluentValidation;
using iRely.MovieRental.Application.Books.Interfaces;
using iRely.MovieRental.Application.Customers.Interfaces;
using iRely.MovieRental.Application.Shared;
using iRely.MovieRental.Application.Books.Commands;
using MediatR;
using iRely.MovieRental.Application.Books.Events;

namespace iRely.MovieRental.Application.Books.Handlers;

public class RentBookToCustomerCommandHandler : IRequestHandler<RentBookToCustomerCommand, Result<bool, ValidationMessages>>
{
    private readonly IBookRepository _bookRepository;
    private readonly ICustomerRepository _customerRepository;
    private readonly IValidator<RentBookToCustomerCommand> _validator;
    private readonly IMediator _mediator;

    public RentBookToCustomerCommandHandler(IBookRepository bookRepository, ICustomerRepository customerRepository, IValidator<RentBookToCustomerCommand> validator, IMediator mediator)
    {
        _bookRepository = bookRepository;
        _customerRepository = customerRepository;
        _validator = validator;
        _mediator = mediator;
    }

    public async Task<Result<bool, ValidationMessages>> Handle(RentBookToCustomerCommand request, CancellationToken cancellationToken)
    {
        var validationResult = await _validator.ValidateAsync(request).ConfigureAwait(false);

        if (validationResult.IsValid)
        {
            var book = await _bookRepository.GetBookByIdAsync(request.BookId).ConfigureAwait(false);
            var customer = await _customerRepository.GetCustomerByIdAsync(request.CustomerId).ConfigureAwait(false);
            book.Rent(customer);
            _bookRepository.Update(book);
            await _bookRepository.SaveChangesAsync().ConfigureAwait(false);

            await _mediator.Publish(new BookRentedToCustomerEvent(customer, book)).ConfigureAwait(false);

            // TODO: Replace return with expected return date
            return true;
        }

        return new ValidationMessages(validationResult.Errors.Select(x => x.ErrorMessage));
    }
}
