﻿using FluentValidation;
using iRely.MovieRental.Application.Books.Commands;
using iRely.MovieRental.Application.Books.Events;
using iRely.MovieRental.Application.Books.Interfaces;
using iRely.MovieRental.Application.Shared;
using MediatR;

namespace iRely.MovieRental.Application.Books.Handlers;

public class ReturnBookCommandHandler : IRequestHandler<ReturnBookCommand, Result<bool, ValidationMessages>>
{
    private readonly IBookRepository _bookRepository;
    private readonly IValidator<ReturnBookCommand> _validator;
    private readonly IMediator _mediator;

    public ReturnBookCommandHandler(IBookRepository bookRepository, IValidator<ReturnBookCommand> validator, IMediator mediator)
    {
        _bookRepository = bookRepository;
        _validator = validator;
        _mediator = mediator;
    }

    public async Task<Result<bool, ValidationMessages>> Handle(ReturnBookCommand request, CancellationToken cancellationToken)
    {
        var validationResult = await _validator.ValidateAsync(request, cancellationToken).ConfigureAwait(false);

        if (validationResult.IsValid)
        {
            var book = await _bookRepository.GetBookByIdAsync(request.BookId).ConfigureAwait(false);
            book.Return();
            _bookRepository.Update(book);
            await _bookRepository.SaveChangesAsync().ConfigureAwait(false);

            await _mediator.Publish(new BookReturnedEvent(book.RentingCustomer, book)).ConfigureAwait(false);

            return true;
        }

        return new ValidationMessages(validationResult.Errors.Select(x => x.ErrorMessage));
    }
}
