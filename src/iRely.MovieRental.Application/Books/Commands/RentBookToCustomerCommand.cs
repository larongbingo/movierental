﻿using iRely.MovieRental.Application.Shared;
using MediatR;

namespace iRely.MovieRental.Application.Books.Commands;

public class RentBookToCustomerCommand : IRequest<Result<bool, ValidationMessages>>
{
    public Guid CustomerId { get; set; }
    public Guid BookId { get; set; }
}
