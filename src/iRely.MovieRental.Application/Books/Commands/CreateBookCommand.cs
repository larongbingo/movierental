﻿using iRely.MovieRental.Application.Shared;
using MediatR;

namespace iRely.MovieRental.Application.Books.Commands;

public class CreateBookCommand : BookCommandBase, IRequest<Result<Guid, ValidationMessages>>
{
}
