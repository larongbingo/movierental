﻿namespace iRely.MovieRental.Application.Books.Commands;

public abstract class BookCommandBase
{
    public string Title { get; set; }
    public string Description { get; set; }
    public Uri? DetailsUri { get; set; }
}
