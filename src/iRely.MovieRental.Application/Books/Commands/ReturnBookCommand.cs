﻿using iRely.MovieRental.Application.Shared;
using MediatR;

namespace iRely.MovieRental.Application.Books.Commands;

public class ReturnBookCommand : IRequest<Result<bool, ValidationMessages>>
{
    public Guid BookId { get; set; }
}
