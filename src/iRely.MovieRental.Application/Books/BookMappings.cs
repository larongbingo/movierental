﻿using iRely.MovieRental.Application.Books.Commands;

namespace iRely.MovieRental.Application.Books;

public static class BookMappings
{
    public static Core.Books.Book CommandToEntity(this CreateBookCommand command)
        => new(command.Title, command.Description, command.DetailsUri);
}
