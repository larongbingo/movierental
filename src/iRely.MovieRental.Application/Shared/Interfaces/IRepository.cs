﻿using iRely.MovieRental.Core.Shared.Interfaces;

namespace iRely.MovieRental.Application.Shared.Interfaces;

public interface IRepository<in T> where T : IEntity
{
    Task<int> SaveChangesAsync();
    void Add(T entity);
    void Update(T entity);
}