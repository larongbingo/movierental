﻿namespace iRely.MovieRental.Application.Shared;

public readonly struct Result<TValue, TError>
{
    private readonly TValue? _value;
    private readonly TError? _error;

    public readonly bool IsSuccess;

    private Result(TError error)
    {
        IsSuccess = false;
        _error = error;
    }

    private Result(TValue value)
    {
        IsSuccess = true;
        _value = value;
    }

    public static implicit operator Result<TValue, TError>(TValue value) => new(value);
    public static implicit operator Result<TValue, TError>(TError error) => new(error);

    public TResult Match<TResult>(Func<TValue, TResult> success, Func<TError, TResult> failure)
    {
        if (IsSuccess)
            return success(_value!);
        else
            return failure(_error!);
    }
}