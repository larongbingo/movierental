﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRely.MovieRental.Application.Shared;

public class ValidationMessages : List<string>
{
    public ValidationMessages(IEnumerable<string> messages) : base(messages)
    {

    }
}
