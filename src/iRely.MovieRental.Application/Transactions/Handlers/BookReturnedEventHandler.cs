﻿using iRely.MovieRental.Application.Books.Events;
using iRely.MovieRental.Application.Transactions.Interfaces;
using MediatR;

namespace iRely.MovieRental.Application.Transactions.Handlers;

public class BookReturnedEventHandler : INotificationHandler<BookReturnedEvent>
{
    private readonly ITransactionRepository _transactionRepository;

    public BookReturnedEventHandler(ITransactionRepository transactionRepository)
    {
        _transactionRepository = transactionRepository;
    }

    public async Task Handle(BookReturnedEvent notification, CancellationToken cancellationToken)
    {
        var newTransaction = notification.EventToReturnedTransaction();
        _transactionRepository.Add(newTransaction);
        await _transactionRepository.SaveChangesAsync();
    }
}
