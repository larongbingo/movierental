﻿using iRely.MovieRental.Application.Books.Events;
using iRely.MovieRental.Application.Transactions.Interfaces;
using MediatR;

namespace iRely.MovieRental.Application.Transactions.Handlers;

public class BookRentedToCustomerEventHandler : INotificationHandler<BookRentedToCustomerEvent>
{
    private readonly ITransactionRepository _transactionRepository;

    public BookRentedToCustomerEventHandler(ITransactionRepository transactionRepository)
    {
        _transactionRepository = transactionRepository;
    }

    public async Task Handle(BookRentedToCustomerEvent notification, CancellationToken cancellationToken)
    {
        var newTransaction = notification.EventToRentedTransaction();
        _transactionRepository.Add(newTransaction);
        await _transactionRepository.SaveChangesAsync().ConfigureAwait(false);
    }
}
