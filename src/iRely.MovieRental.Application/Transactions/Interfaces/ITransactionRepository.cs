﻿using iRely.MovieRental.Application.Shared.Interfaces;
using iRely.MovieRental.Core.Transactions;

namespace iRely.MovieRental.Application.Transactions.Interfaces;

public interface ITransactionRepository : IRepository<Transaction>
{
    Task<IEnumerable<Transaction>> GetTransactionsByCustomerId(Guid customerId);
    Task<IEnumerable<Transaction>> GetTransactionsByBookId(Guid bookId);
}
