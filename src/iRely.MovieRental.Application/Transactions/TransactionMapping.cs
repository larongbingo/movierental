﻿using iRely.MovieRental.Application.Books.Events;
using iRely.MovieRental.Application.Transactions.Handlers;
using iRely.MovieRental.Core.Books;
using iRely.MovieRental.Core.Transactions;

namespace iRely.MovieRental.Application.Transactions;

public static class TransactionMapping
{
    public static Transaction EventToRentedTransaction(this BookRentedToCustomerEvent bookRentedToCustomerEvent)
        => new(TransactionType.Rent, bookRentedToCustomerEvent.Customer, new List<Book>() { bookRentedToCustomerEvent.Book });

    public static Transaction EventToReturnedTransaction(this BookReturnedEvent bookReturnedEvent)
        => new(TransactionType.Return, bookReturnedEvent.Customer, new List<Book>() { bookReturnedEvent.Book });
}
